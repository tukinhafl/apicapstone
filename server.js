const jsonServer = require("json-server");
const auth = require("json-server-auth");

const app = jsonServer.create();
const router = jsonServer.router("db.json");
const port = process.env.PORT || 3005;

app.db = router.db;

const rules = auth.rewriter({
  "/users/*": 664,
  "/clients/*": 640,
  "/professionals": 640
});

app.get('/users/clients', (req, res) => {
  res.jsonp(req.query)
})
app.get('/users/professionals', (req, res) => {
  res.jsonp(req.query)
})

app.use(jsonServer.bodyParser)
app.use((req, res, next) => {
  if (req.method === "POST") {
    req.body.createdAt = Date.now()
  } else if (req.method === "PATCH") {
    req.body.editedAt = Date.now()
  }

  next()
})

app.use(rules);
app.use(auth);
app.use(router);
app.listen(port, () => {
  console.log("Server is running")
});

